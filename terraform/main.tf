# Be aware of AWS Sandbox limits:
# https://support.linuxacademy.com/hc/en-us/articles/360025783132-What-can-I-do-with-the-Cloud-Playground-AWS-Sandbox-

provider "aws" {
  region = var.region
}

module "label" {
  source  = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=tags/0.4.0"

  namespace  = var.namespace
  name       = var.name
  stage      = var.stage
  delimiter  = var.delimiter
  attributes = var.attributes
  tags       = var.tags
}

module "access_logs_label" {
  source  = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=tags/0.4.0"

  namespace  = var.namespace
  name       = var.name
  stage      = var.stage
  delimiter  = var.delimiter
  attributes = ["access", "logs"]
  tags       = var.tags
}

#-----
# DATA
#-----

# You can use LinuxAcademy public VPC if you like
# https://www.terraform.io/docs/providers/aws/d/vpcs.html

data "aws_caller_identity" "this" {}

data "aws_vpcs" "public" {
  tags = {
    Network = "Public"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}